# 使用百度API根据地址批量解析经纬度

#### 介绍
根据百度API，传入相应的地址，返回相应的经纬度数据

使用如图：
![截图](LocationAddress/Images/shortimg.png)
