﻿namespace LocationAddress {
    partial class MainFrm {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent() {
            this.btnGetLocation = new System.Windows.Forms.Button();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.textBoxLocation = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBoxAK = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnGetLocation
            // 
            this.btnGetLocation.Location = new System.Drawing.Point(12, 12);
            this.btnGetLocation.Name = "btnGetLocation";
            this.btnGetLocation.Size = new System.Drawing.Size(75, 23);
            this.btnGetLocation.TabIndex = 0;
            this.btnGetLocation.Text = "获取经纬度";
            this.btnGetLocation.UseVisualStyleBackColor = true;
            this.btnGetLocation.Click += new System.EventHandler(this.btnGetLocation_Click);
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Location = new System.Drawing.Point(12, 42);
            this.textBoxAddress.Multiline = true;
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.Size = new System.Drawing.Size(1006, 241);
            this.textBoxAddress.TabIndex = 1;
            // 
            // textBoxLocation
            // 
            this.textBoxLocation.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.textBoxLocation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxLocation.Location = new System.Drawing.Point(12, 289);
            this.textBoxLocation.Multiline = true;
            this.textBoxLocation.Name = "textBoxLocation";
            this.textBoxLocation.ReadOnly = true;
            this.textBoxLocation.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLocation.Size = new System.Drawing.Size(1006, 220);
            this.textBoxLocation.TabIndex = 2;
            // 
            // textBox3
            // 
            this.textBox3.ForeColor = System.Drawing.Color.Red;
            this.textBox3.Location = new System.Drawing.Point(94, 13);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(206, 21);
            this.textBox3.TabIndex = 3;
            this.textBox3.Text = "如果获取多个经纬度，请用\"，\"分割";
            // 
            // textBoxAK
            // 
            this.textBoxAK.ForeColor = System.Drawing.Color.Red;
            this.textBoxAK.Location = new System.Drawing.Point(379, 15);
            this.textBoxAK.Name = "textBoxAK";
            this.textBoxAK.Size = new System.Drawing.Size(206, 21);
            this.textBoxAK.TabIndex = 4;
            this.textBoxAK.Text = "XBPX8dmIuAzluWeOtTAzsCECpkxkLudV";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(332, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 5;
            this.label1.Text = "AK密钥";
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 521);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxAK);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBoxLocation);
            this.Controls.Add(this.textBoxAddress);
            this.Controls.Add(this.btnGetLocation);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1046, 560);
            this.MinimumSize = new System.Drawing.Size(1046, 560);
            this.Name = "MainFrm";
            this.Text = "根据地址获取经纬度";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGetLocation;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.TextBox textBoxLocation;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBoxAK;
        private System.Windows.Forms.Label label1;
    }
}

