﻿using Newtonsoft.Json;

namespace LocationAddress.Models {
    public class Root {
        /// <summary>
        /// 状态
        /// </summary>
        [JsonProperty("status")]
        public int Status {
            get; set;
        }
        /// <summary>
        /// 结果
        /// </summary>
        [JsonProperty("result")]
        public Result Result {
            get; set;
        }
        /// <summary>
        /// 消息
        /// </summary>
        [JsonProperty("message")]
        public string Message {
            get; set;
        }

        /// <summary>
        /// 消息
        /// </summary>
        [JsonProperty("msg")]
        public string Msg {
            get; set;
        }
    }
}
