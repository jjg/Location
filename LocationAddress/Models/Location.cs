﻿using Newtonsoft.Json;

namespace LocationAddress.Models {
    public class Location {
        /// <summary>
        /// 经度
        /// </summary>
        [JsonProperty("lng")]
        public double Lng {
            get; set;
        }
        /// <summary>
        /// 纬度
        /// </summary>
        [JsonProperty("lat")]
        public double Lat {
            get; set;
        }
    }
}
