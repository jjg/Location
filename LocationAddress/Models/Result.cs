﻿using Newtonsoft.Json;

namespace LocationAddress.Models {
    public class Result {
        /// <summary>
        /// 坐标
        /// </summary>
        [JsonProperty("location")]
        public Location Location {
            get; set;
        }
        /// <summary>
        /// 经度
        /// </summary>
        [JsonProperty("precise")]
        public int Precise {
            get; set;
        }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("confidence")]
        public int Confidence {
            get; set;
        }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("comprehension")]
        public int Comprehension {
            get; set;
        }
        /// <summary>
        /// 汽车服务
        /// </summary>
        [JsonProperty("level")]
        public string Level {
            get; set;
        }
    }
}
